'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');


const ItemSchema = new Schema({
   name: { type: String, trim: true, required: true },
   description: { type: String, trim: true, required: true },
   price: { type: Number, required: true },
   slug: { type: String, trim: true },
   createdById: { type: String, trim: true },
   createdByEmail: { type: String, trim: true },

});
  


ItemSchema.plugin(timestamps);
mongoose.model('Item', ItemSchema);
const Item = mongoose.model('Item', ItemSchema);
module.exports = Item;
