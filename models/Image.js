'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');


const ImageSchema = new Schema({
   itemImg: { type: String, trim: true, required: true },
   forItemId: { type: String, trim: true },
   createdById: { type: String, trim: true },
   createdByEmail: { type: String, trim: true },

});
  


ImageSchema.plugin(timestamps);
mongoose.model('Image', ImageSchema);
const Image = mongoose.model('Image', ImageSchema);
module.exports = Image;
