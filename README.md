# itemizr

REST API final course project from the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.


[![Build Status](https://travis-ci.com/arnelimperial/itemizr.svg?branch=master)](https://travis-ci.com/arnelimperial/itemizr)
[![License: Unlicense](https://img.shields.io/badge/License-Unlicense-9cf.svg)](http://unlicense.org/)
[![Heroku App Status](http://heroku-shields.herokuapp.com/itemizr-api)](https://itemizr-api.herokuapp.com)


By: Arnel Imperial

Student Number: `0618291`


Table of Contents
=================
1. [ Coursework ](#course-work)
2. [ Dependent Project](#dependent-project)
3. [ Getting Started ](#getting-started)
4. [ Structure ](#structure)
5. [ API Endpoints](#endpoints)
6. [Deploy](#deploy)
7. [Video](#video)

<a name="course-work"></a>
## 🎓 Coursework

- REST Intro Module Link: [https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/REST_INTRO_MODULE.md](https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/REST_INTRO_MODULE.md)

- NodeJS Module Link: [https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/NODEJS_MODULE.md](https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/NODEJS_MODULE.md)

- MongoDB Module Link: [https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/MONGODB_MODULE.md](https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/MONGODB_MODULE.md)

- ExpressJS Module Link: [https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/EXPRESSJS_MODULE.md](https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/EXPRESSJS_MODULE.md)

- REST Module Link: [https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/REST_MODULE.md](https://bitbucket.org/arnelimperial/itemizr/src/master/Coursework/REST_MODULE.md)


<a name="dependent-project"></a>
## ⚙ Dependent Project
Side project done after learning the coursework modules

- For Express JS Module: https://bitbucket.org/arnelimperial/itemizr/src/sample-project-2/

- For REST Module: https://bitbucket.org/arnelimperial/itemizr/src/sample-project-1/


<a name="getting-started"></a>
## 😎 Getting Started

__Clone the project from the repository.__

```
$ git clone https://arnelimperial@bitbucket.org/arnelimperial/itemizr.git myproject
$ cd myproject
```

__Install the dependencies from `package.json` using `yarn`__

```
$ yarn
```

__If `npm` is preferred, delete the `yarn.lock` file and `node_modules` folder, then run `npm install`.__

```bash
$ npm install
```

__Create `.env` file at the root of the project directory and setup the `environment variables`.__
The 3 required variables are: `MONGODB`, `JWT_KEY` and `SUPERUSER_ID`.
e.g.

```bash
# .env

MONGODB=mongodb://localhost:27017/mydb # MongoDB URL connection string
JWT_KEY=b2e5943db539285644c838de0015a3ffc2c2cf3a11104adeb280a06cb0ba46f3fc70503d7b5a4d66362050ff01236161 # as secret key
SUPERUSER_ID=607b44e73427bd54995f3cdc # object id of a superuser nominated, can be setup later
```

__Run the code__

```bash
# For yarn
$ yarn run dev

# For NPM
$ npm run dev
```

__To run the test__

```bash
# For yarn
$ yarn run test

# For NPM
$ npm run test
```


<a name="structure"></a>
## ⛑ Structure

```bash
.
├── app.js
├── bin
│   └── www
├── env.yaml
├── LEARNING_DIARY
│   ├── EXPRESSJS_MODULE.md
│   ├── MONGODB_MODULE.md
│   ├── NODEJS_MODULE.md
│   ├── REST_INTRO_MODULE.md
│   └── REST_MODULE.md
├── LICENSE
├── middleware
│   ├── auth.js
│   └── ensureToken.js
├── models
│   ├── Image.js
│   ├── Item.js
│   └── User.js
├── node_modules
├── package.json
├── Procfile
├── public
│   └── stylesheets
│       └── style.css
├── README.md
├── routes
│   ├── home.js
│   ├── image.js
│   ├── items.js
│   └── users.js
├── unittest
│   └── users.test.js
├── uploads
│   └── 2021-05-01T12:34:34.687Z--evaporation_system.jpg
├── views
│   ├── error.ejs
│   └── home.ejs
└── yarn.lock

```

<a name="endpoints"></a>
## 🤖 API Endpoints

### /users resource https://itemizr.herokuapp.com/api/users

Properties:

Object Id: Autofield

username: String data type, required & unique

email: String data type, required & unique valid email

password: String data type, required

confirmationPassword: String data type, required only for verfication to be match with password field

createdAt: Automatically generated date object as created date

updatedAt: Automatically generated date object as date updated


**POST /api/users/register** – Create user

**POST /api/users/login** – Authenticating user   

**GET /api/users(protected route with nominated superuser permission)** – Can fetch all users by superuser

**PATCH /api/users/:username (protected route)** – To update login user's username with username parameter

**PATCH /api/users/email/:id (protected route)** – To update login user's email with object ID as parameter

**PATCH /api/users/password/:id (protected route)** – To update login user's password with object ID as parameter;Password verfication required

**DELETE /api/users/delete/:id (protected route)** – Delete user and all related objects created by user from different resources



### /items resource https://itemizr.herokuapp.com/api/items

Properties:

Object Id: Autofield

name: String data type, required

description: String data type, required

price: Number data type, required

slug: Auto generated String data type

createdAt: Automatically generated date object as created date

updatedAt: Automatically generated date object as date updated

**GET /api/items** – Fetch all items created without authentication

**GET /api/items/by/:email** – Fetch all items created by login user with email parameter

**POST /api/items/create (protected route)** – Create items document with authentication

**PATCH /api/items/name/:slug (protected route)** – Update item's name property with authentication and with slug as parameter

**PATCH /api/items/description/:slug (protected route)** – Update item's description property with authentication and with slug as parameter

**PATCH /api/items/price/:slug (protected route)** – Update item's price property with authentication and with slug as parameter

**DELETE /api/items/delete/:item_id (protected route)** – Delete item's document only by user who created the item with item id as parameter


### /images resource https://itemizr.herokuapp.com/api/images

Properties:

Object Id: Autofield

itemImg: String data type, required; should be a valid file extension(e.g jpeg or png)

forItemId: String data type, auto generated based on the login user's object id

createdAt: Automatically generated date object as created date

updatedAt: Automatically generated date object as date updated

**POST /api/images/create/:item_id (protected route)** – Create image object of the the item;item id from items resource must be provided; Uses form-data for post request 

**GET /api/images/** – List all images created by users

**GET /api/images/by/:email (protected route)** – Fetch all login users created images with user's email as parameter

**DELETE /api/images/delete/:id (protected route)** – To delete image object created by login user


<a name="deploy"></a>
## 💫 Deploy

API project was hosted in [Heroku](https://www.heroku.com/about) with Base URL: [https://itemizr-api.herokuapp.com/](https://itemizr-api.herokuapp.com/)


<a name="video"></a>
## 🧐 Video

View this video: [https://www.youtube.com/watch?v=rEEqxpUu_Pc](https://www.youtube.com/watch?v=rEEqxpUu_Pc) for details about the API in actual testing.

