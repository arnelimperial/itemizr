<h1>REST Module Learning Diary</h1>
Journal of lessons learned from the [Build A REST API With Node.js, Express, & MongoDB - Quick by Web Dev Simplified](https://www.youtube.com/watch?v=fgTGADljAeg) from the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.

By: Arnel Imperial

Student Number: `0618291`


Date: 17.04.2021

## ⚙ What I've done
- To apply what I've learned from the video, I decided to make a simple REST API to test the CRUD operations using NodeJS, Express and MongoDB before starting the final course project. The project that I made was included in the final project repository as orphan branch.

Link to the project source code: [https://bitbucket.org/arnelimperial/itemizr/src/sample-project-1/ (sample-project-1)](https://bitbucket.org/arnelimperial/itemizr/src/sample-project-1/)


The project only has one database collection `services` and http methods(GET, POST, PUT, DELETE) are implemented for RESTful services without users authentication. The properties of `services` collection are:

- _id: Object id
- analyte: String(required POST request)
- method: String(required POST request)
- codeNum: Number(required & unique POST request)
- createdAt: Mongoose timestamp as `created`
- updatedAt: Mongoose timestamp as `updated`

### Directory structure

```bash
.
├── config
│   └── db.js
├── controllers
│   └── services.js
├── env.yaml
├── index.js
├── models
│   └── services.js
├── node_modules
├── package.json
├── README.md
├── routes
│   └── api
│       └── services.js
└── yarn.lock
```

### Test API in POSTMAN

#### Create a service(POST)

![to post](https://od.lk/s/ODhfMTA1NTkxMzVf/toPost_sample1.png)

Response to post request.

![response to post](https://od.lk/s/ODhfMTA1NTkxMzJf/resPost_sample1.png)

Make another post request.

![another post request](https://od.lk/s/ODhfMTA1NTkxMjhf/anotherPost_sample1.png)

and the response:

![another response](https://od.lk/s/ODhfMTA1NTkxMjlf/anotherResponse_sample1.png)


#### Fetch created resources

![display after post](https://od.lk/s/ODhfMTA1NTkxMzFf/get_sample1.png)


#### Update created resource

![to update](https://od.lk/s/ODhfMTA1NTkxMzNf/resUpdate_sample1.png)


#### Get request after update

![after update](https://od.lk/s/ODhfMTA1NTkxMjdf/afterUpdate_sample1.png)


#### Delete request

![to delete](https://od.lk/s/ODhfMTA1NTkxMzRf/toDelete_sample1.png)

#### Get request after delete

![after delete](https://od.lk/s/ODhfMTA1NTkxMjZf/afterDelete_sample1.png)
