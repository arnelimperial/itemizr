<h1>REST Intro Module Learning Diary</h1>
Journal of lessons learned from the [REST API concepts and examples video by WebConcepts](https://www.youtube.com/watch?v=7YcW25PHnAA) from the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.

By: Arnel Imperial

Student Number: `0618291`


Date: 13.04.2021

## 🎓 What I've learned

- Software normally is being used by people, and people are using user interfaces to interact with software. Sometimes other software   applications are also using other software. Such intercommunication expects a different kind of interface, an API(Application Programming Interface). 

- APIs are simplistic, easy to apply, receptive, and clear. By implementing a reusable interface, different applications can associate with them easily. 
Developers are the only people who directly interact with APIs. 

- APIs offer an easy-to-use way for relating to, integrating with, and extending a software system. Presume all the entities and organizations that are run by software. They may have many integrations. Organizations such as banks, companies, stores, schools, etc. They all may have in-house software which works privately. However, when it comes to assimilation, and working with other software in another environment, using APIs is a must thing to do, since APIs give the inclinations which are crucial for such integrations. 

- REST (Representational State Transfer) defines a set of architectural constraints and agreements. We designate an API which is using such a style as a RESTful API. 

- A resource is an abstract data model. A resource is a data structure that can be serialized to multiple concrete descriptions such as XML and JSON. There are connections between Resource and Object-Oriented Programming (OOP) objects, but the difference would be that REST resources are restricted to HTTP methods (GET, POST, PUT, DELETE), but for objects, it would be inconsistent how to use them by the programmer. 

- A resource is an abstraction of an HTTP entity for instance a website. 

- A Representation is a concrete entity, which encodes a resource. The resource can be possible in reoccurring representations such as HTML, XML, or JSON.

- To utilize to create (POST), read (GET), update (PUT), and delete (DELETE) operation to a resource we use Uniform HTTP Interface. 

- The principal objective of APIs is not to return static data. Returned data need to be dynamic according to the input the API receives. We need some sort of communication way to tell the API what we like to receive. By using parameters in the body of the requests we transfer to the API, the API can refine the data and send us what we are searching for.

- In REST, server segments are stateless, and in client components, all the state information is being cached.


