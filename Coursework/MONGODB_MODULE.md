<h1>MongoDB Module Learning Diary</h1>
Journal of lessons learned from the [Mongodb crash course video by Traversy Media](https://www.youtube.com/watch?v=-56x56UppqQ) from
the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.

By: Arnel Imperial

Student Number: `0618291`


Date: 15.04.2021

## ⚙ What I've done
In relation to do the crash video, specifically, the last part of the lesson, I manage to make a simple test on my account in [MongoDB Atlas](https://www.mongodb.com/cloud/atlas/lp/try2?utm_source=google&utm_campaign=gs_emea_finland_search_core_brand_atlas_desktop&utm_term=mongodb%20atlas&utm_medium=cpc_paid_search&utm_ad=e&utm_ad_campaign_id=12212624398&gclid=EAIaIQobChMIkPH46b6r8AIVwqOyCh0McQDXEAAYASAAEgLZovD_BwE) regarding the creation of database. This is important because I want to use implement it on my final course project.

- Install mongodb in my computer

- Create a sandbox project named `Sample Project` and build a shared cluster named `SampleCluster`

![front](https://od.lk/d/ODhfMTA1NDQxNTJf/first.png)

- Create a user from database access panel and enter my current IP address in the network access panel

- Connect with `mongo shell` by pasting the connection string generated from connection tab in the command line
    ```
    $ mongo "mongodb+srv://samplecluster.5ieqp.mongodb.net/sampleDB" --username sampleUser # connection string
    # connection response
    Enter password: 
    connecting to: mongodb://samplecluster-shard-00-00.5ieqp.mongodb.net:27017,samplecluster-shard-00-01.5ieqp.mongodb.net:27017,samplecluster-shard-00-02.5ieqp.mongodb.net:27017/sampleDB?authSource=admin&compressors=disabled&gssapiServiceName=mongodb&replicaSet=atlas-zpbt17-shard-0&ssl=true
    Implicit session: session { "id" : UUID("d61f3dd9-b37a-4ba6-af46-3f71672f5f4b") }
    MongoDB server version: 4.4.5
    ```
- Create database named `sampleDB`
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> use sampleDB
    switched to db sampleDB
    ```

- Create a `samples` collection
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.createCollection('samples')
    {
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1619979883, 1),
		"signature" : {
			"hash" : BinData(0,"9OXTTCCkS8Ffrjqgb2kjGbGjb1k="),
			"keyId" : NumberLong("6957405337968181250")
		}
	},
	"operationTime" : Timestamp(1619979883, 1)
    }
    ```

- Insert data from the collection using `insertMany()`
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.insertMany([{analyte:'Total dissolved solids', method:'Mass of evaporated filtrate', code:123},{analyte:'Electrical conductivity', method: 'Electrical probe', code: 456}, {analyte:'Total nitrogen',method: 'Colorometric method', code:789},{analyte:'Chloride', method:'Discrete analyser',code:101112}])
    {
	    "acknowledged" : true,
	    "insertedIds" : [
		    ObjectId("608ef8c0ad7d0e6f6c36d9ad"),
		    ObjectId("608ef8c0ad7d0e6f6c36d9ae"),
		    ObjectId("608ef8c0ad7d0e6f6c36d9af"),
		    ObjectId("608ef8c0ad7d0e6f6c36d9b0")
	    ]
    }

    ```
- View retrived data
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.find()
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9ad"), "analyte" : "Total dissolved solids", "method" : "Mass of evaporated filtrate", "code" : 123 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9ae"), "analyte" : "Electrical conductivity", "method" : "Electrical probe", "code" : 456 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9af"), "analyte" : "Total nitrogen", "method" : "Colorometric method", "code" : 789 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9b0"), "analyte" : "Chloride", "method" : "Discrete analyser", "code" : 101112 }
    ```

![data](https://od.lk/s/ODhfMTA1NDQxNjFf/second.png)


- Test to update one data entry from the collection, e.g.: Modify the analyte name `Chloride` of code `101112` to **Total particulate    phosphorus**
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.findOneAndUpdate({code:101112},{$set:{analyte:'Total particulate phosphorus'}})
    {
	    "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9b0"),
	    "analyte" : "Chloride",
	    "method" : "Discrete analyser",
	    "code" : 101112
    }

    # check for change
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.find()
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9ad"), "analyte" : "Total dissolved solids", "method" : "Mass of evaporated filtrate", "code" : 123 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9ae"), "analyte" : "Electrical conductivity", "method" : "Electrical probe", "code" : 456 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9af"), "analyte" : "Total nitrogen", "method" : "Colorometric method", "code" : 789 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9b0"), "analyte" : "Total particulate phosphorus", "method" : "Discrete analyser", "code" : 101112 }
    ```

- Test to delete one data entry from the collection, e.g.: Remove the document with _id `608ef8c0ad7d0e6f6c36d9ad`
    ```
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.remove({_id:ObjectId("608ef8c0ad7d0e6f6c36d9ad")})
    WriteResult({ "nRemoved" : 1 })
    MongoDB Enterprise atlas-zpbt17-shard-0:PRIMARY> db.samples.find()
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9ae"), "analyte" : "Electrical conductivity", "method" : "Electrical probe", "code" : 456 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9af"), "analyte" : "Total nitrogen", "method" : "Colorometric method", "code" : 789 }
    { "_id" : ObjectId("608ef8c0ad7d0e6f6c36d9b0"), "analyte" : "Total particulate phosphorus", "method" : "Discrete analyser", "code" : 101112 }
    ```

![result](https://od.lk/s/ODhfMTA1NDQxNzZf/third.png)

