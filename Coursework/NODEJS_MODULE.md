<h1>NodeJS Module Learning Diary</h1>
Journal of lessons learned from the [NodeJS crash course video by Traversy Media](https://www.youtube.com/watch?v=fBNz5xF-Kx4) from
the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.

By: Arnel Imperial

Student Number: `0618291`


Date: 14.04.2021

## 🎓 What I've learned
- To start using NodeJS after installation on my local machine, the first thing to do is to create a **package.json** file by typing `npm init`. The utility will ask a series of question to be filled in.

    ```
    $ npm init

    This utility will walk you through creating a package.json file.
    It only covers the most common items, and tries to guess sensible defaults.

    See `npm help init` for definitive documentation on these fields
    and exactly what they do.

    Use `npm install <pkg>` afterwards to install a package and
    save it as a dependency in the package.json file.

    Press ^C at any time to quit.
    package name: (diary) 
    version: (1.0.0) 
    description: starting node
    entry point: (index.js) 
    test command: 
    git repository: 
    keywords: 
    author: Arnel Imperial
    license: (ISC) 
    About to write to /home/arneli/NodeJS/diary/package.json:

    {
    "name": "hello",
    "version": "1.0.0",
    "description": "starting nodejs",
    "main": "index.js",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    "author": "Arnel Imperial",
    "license": "ISC"
    }


    Is this OK? (yes) 

    ```
- To install a package/s using `npm`, one of the package manager for NodeJS, type:
    ```
    $ npm install <name of the package>  # local installation

    $ npm install -D <name of the package>  # for dev dependencies installation
    ```
- After installing all dependenecies, type:
    ```
    $ npm install
    ```
- The `require()` built-in function is easy to use and function to include packages in the JS files after installation.

- Starting a server is simple in node by using built-in module

    ```javascript
    # index.js

    const http = require('http');

    const host = '127.0.0.1';
    const port = 8080;

    const reqListener = function (req, res) {
        res.writeHead(port, {"Content-Type" : "text/plain"});
        res.end("Hello, World!");
    };

    const server = http.createServer(reqListener);
    server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
    ```

