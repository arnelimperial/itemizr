<h1>ExpressJS Module Learning Diary</h1>
Journal of lessons learned from the [ExpressJS crash course video by Traversy Media](https://www.youtube.com/watch?v=L72fhGm1tfE) from
the subject [Software Development Skills: Backend](https://fitech.io/app/uploads/2020/03/CT70A9130SoftwareDevelopmentSkillsBackEnd-1.pdf) by Lappeenranta–Lahti University of Technology LUT.

By: Arnel Imperial

Student Number: `0618291`


Date: 16.04.2021

## ⚙ What I've done
To apply what I've learned from the crash course video, I decided to do a simple project using NodeJS, Express and MongoDB with the emphasis on how to implement `templating` and `views` in Express application using view engine `EJS`. The project that I made was also includes REST API's CRUD operations and source code was included in the final project repository as orphan branch. Nothing fancy UI was implemented in this project, all I wanted is to render some dynamic data from the Express REST API made and to harness the `MVC` functionality of the technology stacks used. `Axios` was applied to make http request from the API and `method-override` to support `PUT` and `DELETE` methods in the web forms.


Link to the project source code: [https://bitbucket.org/arnelimperial/itemizr/src/sample-project-2/ (sample-project-2)](https://bitbucket.org/arnelimperial/itemizr/src/sample-project-2/)


The project only has one database collection `services` and http methods(GET, POST, PUT, DELETE) are implemented for RESTful services without users authentication. The properties of `services` collection are:

- _id: Object id; __Sample ID__ in view
- analyte: String(required); __Analyte__ in view
- method: String(required); __Test Method__ in view
- codeNum: Number(required & unique); __Code Number__ in view
- createdAt: Mongoose timestamp as `created`; __Date publish__ in view
- updatedAt: Mongoose timestamp as `updated`; __Date updated__ in view


### Project structure
```
.
├── config
│   └── db.js
├── controllers
│   └── services.js
├── env.yaml
├── index.js
├── models
│   └── services.js
├── node_modules
├── package.json
├── routes
│   ├── api
│   │   └── services.js
│   └── index.js
├── views
│   ├── about.ejs
│   ├── index.ejs
│   ├── partials
│   │   ├── head.ejs
│   │   └── header.ejs
│   ├── post.ejs
│   ├── remove.ejs
│   └── update.ejs
└── yarn.lock
```


### API endpoints includes:

GET: **/api/services** – Fetch all services and emanate response such as `_id`, `analyte`, `method`, `codeNum`, and `created` & `update` as timestamp
POST: **/api/services** – Create new service(required properties are `analyte`, `method`, `codeNum`)
PUT: **/api/services/:id** – Modify service properties(partially or full) based on object ID
PUT: **/api/services/:id** – Remove service document based on object ID

### GUI URL includes:
Home page to display sample list: **/**
About page: **/about**
Post form page to add sample/service: **/add**
Update form page to update sample/service based on object ID: **/update/:id**
Delete submission page to delete based on object ID: **/remove/:id**


#### Frontend and Responses
To create the GUI of the app, I installed the EJS package and use to assign a dynamic variables to the html view directory.
```
# index.js
....
app.set('views', './views');
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "public"))); # if using static assets
```

##### Home page "/" to list resources
With two resources of object id as Sample ID `6098dcb2f7bc880f2ad82214` and `609803b56ef0b93b03d3298c` already added.

![home](https://od.lk/s/ODhfMTA1NTg0NDBf/before_update_sample2.png)

##### Select object from the list to update in "/update/:id" with form
The `update button` will redirect the page to update form page.

![to update](https://od.lk/s/ODhfMTA1NTg0NDNf/onUpdate_sample2.png)


All fields in update form are not required and if not field will save the current value of that field property. The response shown below.

![response update](https://od.lk/s/ODhfMTA1NTg0NDZf/resUpdate_sample2.png)

###### List after response

![after update](https://od.lk/s/ODhfMTA1NTg0Mzlf/afterUpdate_sample2.png)


##### Delete selected object in /remove/:id

![On delete](https://od.lk/s/ODhfMTA1NTg0NDJf/onDelete_sample2.png)

###### Response after deleting.

![response delete](https://od.lk/s/ODhfMTA1NTg0NDVf/resDelete_sample2.png)

###### After deleting 

![after delete](https://od.lk/s/ODhfMTA1NTg0Mzdf/afterDelete_sample2.png)


##### Add resource in "/add"
All form fields are required in this page.

![add](https://od.lk/s/ODhfMTA1NTY0MjVf/add_sample2_empty.png)

###### Example to use the add form in "/add"

![to post](https://od.lk/s/ODhfMTA1NTg0NDFf/onadd_sample2.png)

###### Response after post

![response after add](https://od.lk/s/ODhfMTA1NTg0NDRf/resAdd_sample2.png)

###### List added resource in Home page "/"

![after post](https://od.lk/s/ODhfMTA1NTg0MzZf/afterAdd_sample2.png)

##### About page(static page)

![about page](https://od.lk/s/ODhfMTA1NTg4MDVf/about_sample2.png)