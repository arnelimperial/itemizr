process.env.NODE_ENV = "test";
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../app");
const User = require("../models/User");
const dotenv = require("dotenv");
const request = require("supertest");
const expect = require("chai").expect;

dotenv.config();

const user_data = {
  username: "username123",
  email: "user123@example.io",
  password: "password123",
  confirmationPassword: "password123",
};

afterEach((done) => {
  User.collection
    .drop()
    .then(function () {})
    .catch(function () {
      console.warn(" collection may not exists!");
    });
  done();
});

describe("POST /api/users", () => {
  it("should create and login new user", async () => {
    const signup = await request(server)
      .post("/api/users/register")
      .send(user_data);
    expect(signup.status).to.eql(201);
    expect(Array.isArray(signup.body.user_created));
    const user_created = signup.body.user_created;
    expect(user_created).to.include.keys(
      "_id",
      "username",
      "email",
      "password",
      "createdAt",
      "updatedAt"
    );

    const newUser = await User.findOne({ _id: signup.body.user_created._id });

    var new_email = newUser.email;
    var new_password = user_data.password;

    const login = await request(server)
      .post("/api/users/login")
      .send({ email: new_email, password: new_password });
    expect(login.status).to.eql(200);
    expect(Array.isArray(login.body));
    expect(login.body).to.include.keys("message", "token", "user_id");

    let token = login.body.token;

    const update_username = await request(server)
      .patch(`/api/users/${user_data.username}`)
      .set("Authorization", `Bearer ${token}`)
      .send({ username: "username456" });
    expect(update_username.status).to.eql(200);
    const username_updated = update_username.body.username_updated;
    expect(username_updated.username).to.eql("username456");

    const updatedUsername = await User.findOne({
      username: username_updated.username,
    });

    expect(updatedUsername.username).to.eql("username456");

    const delete_user = await request(server)
      .delete(`/api/users/delete/${login.body.user_id}`)
      .set("Authorization", `Bearer ${token}`);
    expect(delete_user.status).to.eql(204);

    const deletedUser = await User.findOne({
      _id: login.body.user_id,
    });

    const deleted_user = await User.find({});
    expect(deleted_user).to.eql([]);
  });
});
