"use strict";

const express = require("express");
const mongoose = require("mongoose");
const User = require("../models/User");
const Joi = require("joi");
const router = express.Router();
const slugify = require("slugify");
const Auth = require("../middleware/auth");
const jwt = require("jsonwebtoken");
const Item = require("../models/Item");
const Image = require("../models/Image");

const randomstring = require("randomstring");
const dotenv = require("dotenv");

dotenv.config();

// add item

router.post("/create", Auth, async (req, res, next) => {
  // get authorization token
  var authorization = req.headers.authorization.split(" ")[1];
  var decoded = jwt.decode(authorization, process.env.JWT_KEY);

  const itemJoiSchema = Joi.object().keys({
    name: Joi.string().required().trim(),
    description: Joi.string().required().trim(),
    price: Joi.number().required().strict().precision(3),
  });

  const result = itemJoiSchema.validate(req.body);

  var values = result.value;

  var { name, description, price } = result.value;

  if (result.error) {
    res.status(422).send({ message: result.error.details[0].message });
  } else {
    const newItem = await new Item({
      name,
      description,
      price,
      slug: slugify(name, { lower: true }) + "-" + randomstring.generate(10),
      createdById: decoded.userId,
      createdByEmail: decoded.email,
    });
    await newItem.save();

    res.status(201).send({ item_created: newItem });
  }
});

// list items

router.get("/", async (req, res, next) => {
  try {
    //exclude id and creator credentials
    let itemList = await Item.find({}, { createdById: 0, createdByEmail: 0 });
    res.send({ Items: itemList });
  } catch (err) {
    res.status(400).send({ error_message: err.message });
  }
});

// list items owned by auth current user

router.get("/by/:email", Auth, async (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userId = decoded.userId;

  var userItem = await Item.find({
    createdById: current_userId,
    createdByEmail: req.params.email,
  });
  if (userItem) {
    res.status(200).send({ Item: userItem });
    console.log(decoded.userId);
  } else {
    res.status(401).send({ error_message: "current user not authorized" });
  }
});

// update item name

router.patch("/name/:slug", Auth, (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userID = decoded.userId;

  var userItem = Item.findOne({ slug: req.params.slug }, function (err, item) {
    const itemNameSchema = Joi.object().keys({
      name: Joi.string().required().trim(),
    });

    // refrain from validating other field_name other than "name" due to unhandled server error
    const updateItemName = itemNameSchema.validate(req.body);

    var slugifyName =
      slugify(updateItemName.value.name, { lower: true }) +
      "-" +
      randomstring.generate(10);

    if (err) {
      res.status(400).send({ error_message: err });
    } else if (item.createdById === current_userID && slugifyName) {
      const updateItem = Item.findOneAndUpdate(
        { slug: req.params.slug },
        {
          $set: {
            name: updateItemName.value.name,
            slug: slugifyName,
          },
        },
        { new: true },
        function (err, doc) {
          if (updateItemName.error) {
            return res.status(400).send({
              error_message: updateItemName.error.details[0].message,
            });
          } else if (err) {
            return res.status(500).send({ error_message: err });
          } else {
            return res.status(200).send({ item_name_updated: doc });
          }
        }
      );
    } else if (item.createdById !== current_userID) {
      return res
        .status(403)
        .send({ error_message: "user not allowed to modify the document" });
    } else if (updateItemName.error) {
      return res
        .status(400)
        .send({ error_message: updateItemName.error.details[0].message });
    } else {
      return res.status(500).send({ error_message: "Big error " });
    }
  });
});
// update item description

router.patch("/description/:slug", Auth, (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userID = decoded.userId;

  var userItem = Item.findOne({ slug: req.params.slug }, function (err, item) {
    const itemDescriptionSchema = Joi.object().keys({
      description: Joi.string().required().trim(),
    });

    if (item.createdById === current_userID) {
      const updateItemDescription = itemDescriptionSchema.validate(req.body);
      const updateItem = Item.findOneAndUpdate(
        { slug: req.params.slug },
        {
          $set: {
            description: updateItemDescription.value.description,
          },
        },
        { new: true },
        function (err, doc) {
          if (updateItemDescription.error) {
            return res.status(400).send({
              error_message: updateItemDescription.error.details[0].message,
            });
          } else {
            return res.status(200).send({ item_description_updated: doc });
          }
        }
      );
    } else if (err) {
      res.status(400).send({ error_message: err });
    } else if (item.createdById !== current_userID) {
      return res
        .status(403)
        .send({ error_message: "user not allowed to modify the document" });
    }
  });
});

// update item price

router.patch("/price/:slug", Auth, (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userID = decoded.userId;

  var userItem = Item.findOne({ slug: req.params.slug }, function (err, item) {
    const itemPriceSchema = Joi.object().keys({
      price: Joi.number().required().strict().precision(3),
    });

    if (item.createdById === current_userID) {
      const updateItemPrice = itemPriceSchema.validate(req.body);
      const updateItem = Item.findOneAndUpdate(
        { slug: req.params.slug },
        {
          $set: {
            price: updateItemPrice.value.price,
          },
        },
        { new: true },
        function (err, doc) {
          if (updateItemPrice.error) {
            return res.status(400).send({
              error_message: updateItemPrice.error.details[0].message,
            });
          } else {
            return res.status(200).send({ item_price_updated: doc });
          }
        }
      );
    } else if (err) {
      res.status(400).send({ error_message: err });
    } else if (item.createdById !== current_userID) {
      return res
        .status(403)
        .send({ error_message: "user not allowed to modify the document" });
    }
  });
});

// delete item owned by current auth user only

router.delete("/delete/:itemId", Auth, async (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userID = decoded.userId;

  var itemSearch = await Item.findOne({ _id: req.params.itemId });
  if (itemSearch.createdById === current_userID) {
    try {
      let deleteImages = await Image.deleteMany({
        forItemId: req.params.itemId,
      });
      let deleteItem = await Item.findOneAndDelete({ _id: req.params.itemId });
      res.status(204).send({ message: "item and related objects deleted" });
      next();
    } catch (err) {
      next(err);
    }
  } else {
    res.status(403).send({
      error_message: "current user has no permission to perform delete action",
    });
  }
});

module.exports = router;
