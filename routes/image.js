"use strict";
const dotenv = require("dotenv");
const express = require("express");
const mongoose = require("mongoose");
const Joi = require("joi");
const router = express.Router();
const Auth = require("../middleware/auth");
const jwt = require("jsonwebtoken");
const Image = require("../models/Image");
const User = require("../models/User");
const Item = require("../models/Item");
const randomstring = require("randomstring");


const multer = require("multer");

dotenv.config();

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "uploads/");
  },
  filename: function (req, file, callback) {
    callback(null, new Date().toISOString() +'--'+ file.originalname);
  },
});
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};

const upload = multer({ storage: storage, fileFilter: fileFilter });



// add item

router.post("/create/:itemId", Auth, upload.single("itemImg"), async (req, res, next) => {
  // get authorization token
  var authorization = req.headers.authorization.split(" ")[1];
  var decoded = jwt.decode(authorization, process.env.JWT_KEY);

  const imageJoiSchema = Joi.object().keys({
   itemImg: Joi.string().trim(),
  });

  const result = imageJoiSchema.validate(req.body);

  if (result.error) {
    res.status(400).send({ message: result.error.details[0].message });
  } else {
    const newImg = await new Image({
      itemImg: req.file.path,
      forItemId: req.params.itemId,
      createdById: decoded.userId,
      createdByEmail: decoded.email,
    });
    await newImg.save();

    res.status(201).send({ image_created: newImg });
   
  }
});


// list images

router.get("/", async (req, res, next) => {
  try {
    //exclude id and creator credentials
    let imageList = await Image.find({}, { createdById: 0, createdByEmail: 0, forItemId: 0 });
    res.send({ Images: imageList });
  } catch (err) {
    res.status(400).send({ error_message: err.message });
  }
});

// list images owned by auth current user

router.get("/by/:email", Auth, async (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userId = decoded.userId;

  var userImage = await Image.find({
    createdById: current_userId,
    createdByEmail: req.params.email,
  });
  if (userImage) {
    res.status(200).send({ Image: userImage });
    console.log(decoded.userId);
  } else {
    res.status(401).send({ error_message: "current user not authorized" });
  }
});


// delete image created by user


router.delete("/delete/:id", Auth, async (req, res, next) => {
  let authorization = req.headers.authorization.split(" ")[1];
  let decoded = jwt.decode(authorization, process.env.JWT_KEY);
  let current_userID = decoded.userId;

  let userImage = await Image.findOne({
    _id: req.params.id,
    createdById: current_userID,
  });

  if (userImage) {
    Image.findOneAndDelete({
      _id: req.params.id,
      createdById: current_userID,
    })
      .exec()
      .then((docs) => {
        res.status(200).send({ message: "image deleted" });
      })
      .catch((err) => {
        res.status(400).send({ error_message: err });
      });
  } else {
    res.status(403).send({ error_message: "current user not authorized" });
  }
});


module.exports = router;
