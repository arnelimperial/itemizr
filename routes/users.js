"use strict";

const express = require("express");
const mongoose = require("mongoose");
const User = require("../models/User");
const Joi = require("joi");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Auth = require("../middleware/auth");
const Item = require("../models/Item");
const Image = require("../models/Image");
const dotenv = require("dotenv");

dotenv.config();



//validation Schema

router.get("/", Auth, async (req, res, next) => {
  // get authorization token from header
  var authorization = req.headers.authorization.split(" ")[1];
  try {
    var user = await User.find({}, "username email updatedAt createdAt");
    // decode the jwt
    var decoded = jwt.decode(authorization, process.env.JWT_KEY);
  } catch (err) {
    return res.status(403).send("restricted route");
  }
  // only superuser can access this route
  if (decoded.userId === process.env.SUPERUSER_ID) {
    return res.send({ User: user });
  } else {
    return res.status(403).send("restricted route");
  }
});

// User Sign up

router.post("/register", async (req, res, next) => {
  const userPostSchema = Joi.object().keys({
    username: Joi.string().required().trim().min(5).max(20),
    email: Joi.string().required().email().trim().strict(),
    password: Joi.string().min(8).required().strict(),
    confirmationPassword: Joi.string()
      .valid(Joi.ref("password"))
      .required()
      .strict(),
  });
  try {
    const signupResult = userPostSchema.validate(req.body);
    const user = await User.findOne({ email: signupResult.value.email });

    if (signupResult.error) {
      res
        .status(400)
        .send({ error_message: signupResult.error.details[0].message });
    } else if (user) {
      res.status(422).send({ error_message: "Email is already in use." });
    } else {
      const hash = await User.hashPassword(signupResult.value.password);
      delete signupResult.value.confirmationPassword;
      signupResult.value.password = hash;

      const newUser = await new User(signupResult.value);
      await newUser.save();
      res.status(201).send({ user_created: newUser });
      console.log(newUser);
    }
  } catch (err) {
    next(err);
  }
});

router.post("/login", (req, res, next) => {
  const userLoginSchema = Joi.object().keys({
    email: Joi.string().required().email().trim().strict(),
    password: Joi.string().min(8).required().strict(),
  });
  const loginResult = userLoginSchema.validate(req.body);
  if (loginResult.error) {
    res
      .status(422)
      .send({ error_message: loginResult.error.details[0].message });
  } else if (loginResult) {
    User.find({ email: req.body.email })
      .exec()
      .then((doc) => {
        if (doc.length < 1) {
          return res
            .status(401)
            .json({ error_message: "Authentication failed" });
        }
        bcrypt.compare(req.body.password, doc[0].password, (err, results) => {
          if (err) {
            return res
              .status(401)
              .json({ error_message: "Authentication failed" });
          }
          if (results) {
            const token = jwt.sign(
              {
                email: doc[0].email,
                userId: doc[0]._id,
              },
              process.env.JWT_KEY,
              { expiresIn: "1h" }
            );

            let decode = jwt.decode(token, process.env.JWT_KEY);
            let user_id = decode.userId;
            return res.status(200).json({
              message: "Authentication successful",
              token: token,
              user_id: user_id,
            });
          
          }
          res.status(401).json({ error_message: "Authentication failed3" });
        });
      })
      .catch((err) => {
        res.status(500).json({ Error_Message: err.message });
      });
  }
});

// update user' username by current auth user

router.patch("/:username", Auth, async (req, res, next) => {
  //Unencrypted password will be save in DB if updated

  const usernameUpdateSchema = Joi.object().keys({
    username: Joi.string().trim().min(5).max(20),
  });

  const updateUsername = usernameUpdateSchema.validate(req.body);
  const duplicateUsername = await User.findOne({
    username: updateUsername.value.username,
  });

  if (updateUsername.error) {
    res
      .status(422)
      .send({ error_message: updateUsername.error.details[0].message });
  } else if (duplicateUsername) {
    res.status(422).send({ error_message: "Username is already in use." });
  } else if (updateUsername) {
    try {
      const updateUser = await User.findOneAndUpdate(
        { username: req.params.username },
        {
          $set: {
            username: updateUsername.value.username,
          },
        }
      );
      const updatedUsername = await User.findOne({
        username: updateUsername.value.username,
      });

      res.status(200).send({ username_updated: updatedUsername });
      console.log(updateUser);
    } catch (err) {
      res.send({ error_message: err.message });
    }
  }
});

// update users email

router.patch("/email/:id", Auth, async (req, res, next) => {
  const emailUpdateSchema = Joi.object().keys({
    email: Joi.string().email().trim().strict(),
  });

  const updateEmail = emailUpdateSchema.validate(req.body);
  const duplicateEmail = await User.findOne({ email: updateEmail.value.email });

  if (updateEmail.error) {
    res
      .status(422)
      .send({ error_message: updateEmail.error.details[0].message });
  } else if (duplicateEmail) {
    res.status(422).send({ error_message: "Email is already in use." });
  } else if (updateEmail) {
    try {
      const updateUser = await User.findOneAndUpdate(
        { _id: req.params.id },
        {
          $set: {
            email: updateEmail.value.email,
          },
        }
      );
      const updatedEmail = await User.findOne({
        email: updateEmail.value.email,
      });

      res.status(200).send({ email_updated: updatedEmail });
    } catch (err) {
      res.send({ error_message: err.message });
    }
  }
});

// update users password

router.patch("/password/:id", Auth, async (req, res, next) => {
  const passwordUpdateSchema = Joi.object().keys({
    password: Joi.string().min(8).required().strict(),
    confirmationPassword: Joi.string()
      .valid(Joi.ref("password"))
      .required()
      .strict(),
  });

  const updatePassword = passwordUpdateSchema.validate(req.body);

  if (updatePassword.error) {
    res
      .status(422)
      .send({ error_message: updatePassword.error.details[0].message });
  } else if (updatePassword) {
    const hashNewPassword = await User.hashPassword(
      updatePassword.value.password
    );
    delete updatePassword.value.confirmationPassword;
    updatePassword.value.password = hashNewPassword;

    try {
      const updateUser = await User.findOneAndUpdate(
        { _id: req.params.id },
        {
          $set: {
            password: updatePassword.value.password,
          },
        }
      );
      const updatedPassword = await User.findOne({ _id: req.params.id });

      res.status(200).send({ password_updated: updatedPassword });
    } catch (err) {
      res.send({ error_message: err });
    }
  }
});

// delete auth user

router.delete("/delete/:id", Auth, async (req, res, next) => {
  const authorization = req.headers.authorization.split(" ")[1];
  const decoded = jwt.decode(authorization, process.env.JWT_KEY);
  const current_userId = decoded.userId;
  if (current_userId === req.params.id) {
    try {
      let items = await Item.deleteMany({ createdById: current_userId });
      let images = await Image.deleteMany({ createdById: current_userId });
      let user = await User.findByIdAndDelete({ _id: req.params.id });
      res.status(204).send({ message: "user related objects deleted" });
      next();
    } catch (err) {
      return next(err);
    }
  } else {
    res.status(403).send({ error_message: "no permission to delete account and related objects" });
  }
});

module.exports = router;
