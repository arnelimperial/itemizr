"use strict";
require("dotenv").config();

const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const helmet = require("helmet");
const cors = require("cors");

const homeRouter = require("./routes/home");
const userRouter = require("./routes/users");
const itemRouter = require("./routes/items");
const imageRouter = require("./routes/image");

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// to log http requests
app.use(logger("dev"));
// to view static media uploads
app.use("/uploads", express.static("uploads"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
// secure express api by defining http headers
app.use(helmet());

// to add headers stating that the api accepts requests coming from other origin
app.use(cors());

app.use(require("sanitize").middleware);

// app.use(bodyParser.json());

app.use("/", homeRouter);
app.use("/api/users", userRouter);
app.use("/api/items", itemRouter);
app.use("/api/images", imageRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

// Database

let db_uri;
const db_options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
};
if (process.env.NODE_ENV === "development") {
  db_uri = process.env.MONGODB;
}
if (process.env.NODE_ENV === "test") {
  db_uri = process.env.MONGODB_TEST;
}
if (process.env.NODE_ENV === "production") {
  db_uri = process.env.MONGODB;
}

mongoose.connect(db_uri, db_options).then(
  () => {
    console.log("Database connected");
  },
  (err) => {
    console.log(err);
  }
);

module.exports = app;
